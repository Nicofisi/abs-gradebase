package me.nicofisi.gradebase

import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class StudentService(
    private val studentRepository: StudentRepository,
) {
    fun getStudent(index: Int): Student {
        return studentRepository.findByIdOrNull(index) ?: throw StudentNotFoundException(index)
    }

    fun getAllStudents(): List<Student> {
        return studentRepository.findAll().toList()
    }

    fun createStudent(index: Int, studentUpsertDto: StudentUpsertDto): Student {
        if (studentRepository.existsById(index)) {
            throw StudentAlreadyExistsException(index)
        }

        val student = Student(
            index = index,
            firstName = studentUpsertDto.firstName,
            lastName = studentUpsertDto.lastName,
            gradesByCourse = emptyMap(),
        )

        studentRepository.save(student)

        return student
    }

    fun updateStudent(index: Int, studentUpdateDto: StudentUpsertDto): Student {
        val student =
            studentRepository.findByIdOrNull(index) ?: throw StudentNotFoundException(index)

        val updatedStudent = student.copy(
            firstName = studentUpdateDto.firstName,
            lastName = studentUpdateDto.lastName,
        )

        studentRepository.save(updatedStudent)

        return updatedStudent
    }

    fun deleteStudent(index: Int) {
        if (!studentRepository.existsById(index)) {
            throw StudentNotFoundException(index)
        }

        studentRepository.deleteById(index)
    }

    fun upsertStudentCourseGrade(index: Int, bodyPart: String, grade: Int): Student {
        val student =
            studentRepository.findByIdOrNull(index) ?: throw StudentNotFoundException(index)

        if (grade !in 0..100) {
            throw IllegalGradeValueException(grade)
        }

        val updatedStudent = student.copy(
            gradesByCourse = student.gradesByCourse + (bodyPart to grade)
        )

        studentRepository.save(updatedStudent)

        return updatedStudent
    }

    fun deleteStudentCourseGrade(index: Int, bodyPart: String): Student {
        val student =
            studentRepository.findByIdOrNull(index) ?: throw StudentNotFoundException(index)

        val updatedStudent = student.copy(
            gradesByCourse = student.gradesByCourse - bodyPart
        )

        studentRepository.save(updatedStudent)

        return updatedStudent
    }
}
