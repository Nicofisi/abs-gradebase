package me.nicofisi.gradebase

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.NOT_FOUND)
class StudentNotFoundException(index: Int) :
    RuntimeException("Student with given index not found, index: $index")

@ResponseStatus(value = HttpStatus.NOT_FOUND)
class StudentAlreadyExistsException(index: Int) :
    RuntimeException("Student with given index already exists, index: $index")

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
class IllegalGradeValueException(value: Int) :
    RuntimeException("The grade must be between 0 and 100, but was: $value")
