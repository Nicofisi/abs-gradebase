package me.nicofisi.gradebase

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Student(
    @Id
    val index: Int,
    val firstName: String,
    val lastName: String,
    val gradesByCourse: Map<String, Int>,
)

data class StudentUpsertDto(
    val firstName: String,
    val lastName: String,
)
