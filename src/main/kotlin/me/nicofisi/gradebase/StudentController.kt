package me.nicofisi.gradebase

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/students")
class StudentController(
    private val studentService: StudentService
) {
    @GetMapping
    fun getAllStudents(): List<Student> {
        return studentService.getAllStudents()
    }

    @GetMapping("/{index}")
    fun getStudent(@PathVariable index: Int): Student {
        return studentService.getStudent(index)
    }

    @PutMapping("/{index}")
    fun createStudent(
        @RequestBody studentUpsertDto: StudentUpsertDto,
        @PathVariable index: Int
    ): ResponseEntity<Student> {
        val student = studentService.createStudent(index, studentUpsertDto)

        return ResponseEntity.status(HttpStatus.CREATED).body(student)
    }

    @PatchMapping("/{index}")
    fun updateStudent(
        @PathVariable index: Int,
        @RequestBody studentUpsertDto: StudentUpsertDto
    ): Student {
        return studentService.updateStudent(index, studentUpsertDto)
    }

    @DeleteMapping("/{index}")
    fun deleteStudent(@PathVariable index: Int) {
        studentService.deleteStudent(index)
    }

    @PutMapping("/{index}/course-grades/{courseName}")
    fun upsertStudentCourseGrade(
        @PathVariable index: Int, @PathVariable courseName: String, @RequestBody grade: Int
    ): Student {
        return studentService.upsertStudentCourseGrade(index, courseName, grade)
    }

    @DeleteMapping("/{index}/course-grades/{courseName}")
    fun deleteStudentCourseGrade(
        @PathVariable index: Int, @PathVariable courseName: String,
    ) {
        studentService.deleteStudentCourseGrade(index, courseName)
    }

}
