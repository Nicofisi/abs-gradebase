package me.nicofisi.gradebase

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GradebaseApplication

fun main(args: Array<String>) {
    runApplication<GradebaseApplication>(*args)
}
