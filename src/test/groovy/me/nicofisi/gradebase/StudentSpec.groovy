package me.nicofisi.gradebase

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.test.context.ActiveProfiles
import spock.lang.Shared
import spock.lang.Specification

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import static org.springframework.http.HttpMethod.PATCH
import static org.springframework.http.HttpMethod.PUT
import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.OK

@ActiveProfiles(profiles = "test")
@SpringBootTest(webEnvironment = RANDOM_PORT)
class StudentSpec extends Specification {

    @Autowired
    TestRestTemplate testRestTemplate

    @Autowired
    StudentRepository studentRepository

    def setup() {
        testRestTemplate.getRestTemplate().setRequestFactory(new HttpComponentsClientHttpRequestFactory())
        studentRepository.deleteAll()
    }

    def "should be able to get a student"() {
        given:
        def repoStudent = new Student(123451, "Jan", "Kowalski", [:])
        studentRepository.save(repoStudent)

        when:
        def response = testRestTemplate.getForEntity("/students/123451", Student)

        then:
        response.statusCode == OK

        and:
        response.body == repoStudent
    }

    def "should be able to create a student"() {
        given:
        def payload = new StudentUpsertDto("Jan", "Kowalski")

        when:
        def response = testRestTemplate.exchange(
                "/students/123456",
                PUT,
                new HttpEntity(payload),
                Student)

        then:
        response.statusCode == CREATED

        then:
        def student = response.body

        with(student) {
            index == 123456
            firstName == "Jan"
            lastName == "Kowalski"
            gradesByCourse == [:]
        }

        then:
        studentRepository.findAll().size() == 1
        def studentFromDb = studentRepository.findById(123456).get()

        with(studentFromDb) {
            index == 123456
            firstName == "Jan"
            lastName == "Kowalski"
            gradesByCourse == [:]
        }
    }

    def "should be able to update a student"() {
        given:
        def existingStudent = new Student(123454, "Jan", "Kowalski", [:])
        studentRepository.save(existingStudent)

        def payload = new StudentUpsertDto("Janina", "Nowak")

        when:
        def response = testRestTemplate.exchange(
                "/students/123454",
                PATCH,
                new HttpEntity(payload),
                Student.class)

        then:
        response.statusCode == OK

        then:
        def student = response.body

        with(student) {
            index == 123454
            firstName == "Janina"
            lastName == "Nowak"
            gradesByCourse == [:]
        }

        then:
        studentRepository.findAll().size() == 1
        def studentFromDb = studentRepository.findById(123454).get()

        with(studentFromDb) {
            index == 123454
            firstName == "Janina"
            lastName == "Nowak"
            gradesByCourse == [:]
        }
    }

    def "should be able to delete student"() {
        given:
        def student = new Student(123457, "Jan", "Kowalski", [:])
        studentRepository.save(student)

        when:
        testRestTemplate.delete("/students/123457")

        then:
        studentRepository.findAll().size() == 0
    }

}
