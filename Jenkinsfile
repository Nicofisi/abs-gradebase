pipeline {
    agent any

    tools {
        jdk 'jdk11'
    }

    environment {
        DATE = new Date().format('yy.M')
        TAG = "${DATE}.${BUILD_NUMBER}"
    }

    options {
        timeout(10)
        gitLabConnection('GitLab')
    }

    triggers {
        gitlab(
            triggerOnPush: true,
            triggerOnMergeRequest: true,
            branchFilterType: 'All',
            addVoteOnMergeRequest: true)
    }

    stages {
        stage('Build with Gradle') {
            steps {
                sh './gradlew --no-daemon build'
            }
        }

        stage('Docker Build') {
            steps {
                script {
                    docker.build("nicostudia/gradebase-api:${TAG}")
                }
            }
        }

        stage('Push Docker Image to Dockerhub') {
            when {
                branch 'main'
            }

            steps {
                script {
                    docker.withRegistry('https://registry.hub.docker.com', 'docker_credential') {
                        docker.image("nicostudia/gradebase-api:${TAG}").push()
                        docker.image("nicostudia/gradebase-api:${TAG}").push('latest')
                    }
                }
            }
        }
    }
}
