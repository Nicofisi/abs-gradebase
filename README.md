Większość ciekawych plików, w tym te od k8s, jest w folderze `docker`.

## More info

This project allows saving information about students.

The information is:
- index
- firstName
- lastName
- a map/dictionary of courseName -> grade

## What works
There is a docker-compose-dev file that starts a Spring Boot application with a MongoDB database.

Pushing commits to https://gitlab.com/Nicofisi/abs-gradebase/ triggers a webhook that starts a Jenkins pipeline.

### docker-compose-dev.yml
I run this with two environment variables - `MONGO_PASSWORD` and `MONGO_INITDB_ROOT_PASSWORD`. The first one is used to set the password for the user that is created in the database. The second one is used to set the password for the root user. The root user is used to create the user that is used by the application (and can be used in command line to connect to the database).

I run docker-compose with the `--build` flag, so the image is built every time I run it.

### Uruchamianie k8s, przykład

W pierwszej kolejności należy zainstalować Docker Desktop i aktywować
w nim Kubernetes lub w inny sposób pozyskać komendy `docker` i `kubectl`.

Dalej, należy sklonować gdzieś repozytorium `mongodb-kubernetes-operator` i
wykonać następujące kroki:

```bash
git clone https://github.com/mongodb/mongodb-kubernetes-operator.git
cd mongodb-kubernetes-operator
git checkout tags/v0.7.4
kubectl apply -f config/crd/bases/mongodbcommunity.mongodb.com_mongodbcommunity.yaml
kubectl create namespace mongo
kubectl apply -k config/rbac/ --namespace mongo
kubectl create -f config/manager/manager.yaml --namespace mongo
```

Następnie, należy zastosować wszystkie konfiguracje z folderu `k8s/prod` lub
`k8s/dev`, modyfikując odpowiednio następującą komendę:

```bash
kubectl apply -rf k8s/..........
```
