FROM gradle:7.6.0-jdk11-alpine AS build
ENV APP_HOME=/usr/app/
WORKDIR $APP_HOME

COPY . .
RUN gradle clean bootJar

FROM eclipse-temurin:11-jre-alpine
ENV ARTIFACT_NAME=gradebase-1.0.0.jar
ENV APP_HOME=/usr/app/

WORKDIR $APP_HOME
COPY --from=build $APP_HOME/build/libs/$ARTIFACT_NAME .

EXPOSE 8080
ENTRYPOINT exec java -jar ${ARTIFACT_NAME}

